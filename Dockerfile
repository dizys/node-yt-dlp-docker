FROM node:latest

RUN set -eux; \
    apt-get update; \
    apt-get -y install ffmpeg

RUN set -eux; \
    curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp; \
    chmod a+rx /usr/local/bin/yt-dlp; \
    ln -s /usr/local/bin/yt-dlp /usr/local/bin/youtube-dl
